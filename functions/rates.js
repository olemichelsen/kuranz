const crypto = require('crypto')
const fs = require('fs')
const https = require('https')

const host = 'openexchangerates.org'
const path = `/api/latest.json?app_id=${process.env.API_KEY}`
const url = `https://${host}${path}`

const md5 = (str) => crypto.createHash('md5').update(String(str)).digest('hex')

const cacheFilename = `/tmp/${md5(url)}.json`
const cacheTimeout = 60 * 60 * 1000

const readCache = () => (
	new Promise((resolve) => (
		fs.readFile(cacheFilename, 'utf8', (err, data) => {
			if (err) {
				console.log('cache.miss', err.message)
				resolve()
			} else {
				const json = JSON.parse(data)

				if (json.timestamp * 1000 > Date.now() - cacheTimeout) {
					console.log('cache.expired', { timestamp: json.timestamp, now: Date.now() })
					resolve()
				} else {
					console.log('cache.hit', { timestamp: json.timestamp })
					resolve(json)
				}
			}
		})
	))
)

const writeCache = (data) => (
	new Promise((resolve) => (
		fs.writeFile(cacheFilename, JSON.stringify(data), 'utf8', (err) => {
			if (err) {
				console.error('cache.write', err.message)
			}
			resolve(data)
		})
	))
)

const fetchAndStoreData = () => (
	new Promise((resolve, reject) => {
		console.log('fetch', url)
		https.get({
			host,
			path,
			headers: {
				'Content-Type': 'application/json',
				'Accept': 'application/json'
			},
		}, (res) => {
			if (res.statusCode !== 200) {
				res.resume()
				return reject(new Error(`fetch statusCode=${res.statusCode} statusMessage=${res.statusMessage}`))
			}

			let raw = ''
			res.setEncoding('utf8')
			res.on('data', (chunk) => raw += chunk)
			res.on('end', () => resolve(JSON.parse(raw)))
		}).on('error', (err) => reject(err))
	}).then(writeCache)
)

exports.handler = (event, context, callback) => {
	readCache()
		.then((data) => data ? data : fetchAndStoreData())
		.then((data) => {
			const cacheTimestamp = data.timestamp * 1000
			const cacheEtag = md5(cacheTimestamp)
			let ifModifiedSince
			let etagHeader

			try {
				if (event.headers['if-modified-since']) {
					ifModifiedSince = new Date(event.headers['if-modified-since']).getTime()
				}
				if (event.headers['if-none-match']) {
					etagHeader = event.headers['if-none-match']
				}
			} catch (err) {
				console.error('etag', err.message)
			}

			if (ifModifiedSince === cacheTimestamp || etagHeader === cacheEtag) {
				callback(null, {
					statusCode: 304
				})
			} else {
				callback(null, {
					statusCode: 200,
					headers: {
						'Content-Type': 'application/json; chartset=utf-8',
						'Access-Control-Allow-Origin': '*',
						'Cache-Control': `public, max-age=${cacheTimeout / 1000}`,
						'Last-Modified': new Date(cacheTimestamp).toUTCString(),
						'Expires': new Date(cacheTimestamp + cacheTimeout).toUTCString(),
						'ETag': cacheEtag,
					},
					body: JSON.stringify(data)
				})
			}
		})
		.catch((err) => {
			console.error(err.message)
			callback(err)
		})
}
