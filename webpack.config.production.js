const CopyWebpackPlugin = require('copy-webpack-plugin')
const OfflinePlugin = require('offline-plugin')
const { resolve } = require('path')
const config = require('./config')

module.exports = {
	output: {
		filename: '[name]-[chunkhash:6].js',
		path: resolve(__dirname, 'public'),
		publicPath: '/',
	},
	plugins: [
		new CopyWebpackPlugin([
			'./src/_headers',
		]),
		new OfflinePlugin({
			caches: {
				main: ['app/*.html', '*.js', 'data/*.json', '*.woff2'],
				additional: [':externals:'],
			},
			externals: [
				'/app/',
				config.data.currencies,
				config.data.rates,
			],
			responseStrategy: 'network-first',
			safeToUseOptionalCaches: true,
			ServiceWorker: {
				events: true,
				minify: false,
				navigateFallbackURL: '/app/',
			},
			AppCache: {
				events: true,
				caches: ['main', 'optional'],
			},
		}),
	],
	devtool: 'source-map',
}
