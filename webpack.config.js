const webpackMerge = require('webpack-merge')
const MiniCssExtractPlugin = require('mini-css-extract-plugin')
const HtmlWebpackPlugin = require('html-webpack-plugin')
const HtmlWebpackInlineSourcePlugin = require('html-webpack-inline-source-plugin')
const WebpackPwaManifest = require('webpack-pwa-manifest')
const { resolve } = require('path')

module.exports = (_, { mode: env = 'development' }) => webpackMerge({
	entry: {
		index: './src/index.ts',
		app: './src/app.tsx',
	},
	output: {
		filename: '[name].js',
		path: resolve(__dirname, 'app'),
		publicPath: '/',
	},
	resolve: {
		extensions: ['.js', '.json', '.ts', '.tsx'],
	},
	module: {
		rules: [
			{ test: /\.tsx?$/, loader: 'ts-loader' },
			{ test: /\.(gif|ico|jpg|png|svg)$/, use: ['file-loader?name=img/[name]-[hash:6].[ext]'] },
			{ test: /(\.mp4)$/, use: ['file-loader?name=[name]-[hash:6].[ext]'] },
			{ test: /\.json$/, type: 'javascript/auto', include: /\/data\//, use: ['file-loader?name=data/[name]-[hash:6].[ext]'] },
			{ test: /\.woff2$/, use: ['url-loader?limit=8192&name=[name]-[hash:6].[ext]'] },
			{
				test: /\.less$/,
				use: [
					env === 'development' ? 'style-loader' : MiniCssExtractPlugin.loader,
					'typings-for-css-modules-loader?importLoaders=1&modules&namedExport&camelCase&minimize&silent',
					{
						loader: 'postcss-loader',
						options: {
							sourceMap: env !== 'production',
							plugins: () => [require('autoprefixer')()],
						},
					},
					{
						loader: 'less-loader',
						options: { sourceMap: env !== 'production' },
					},
				],
			},
		],
	},
	plugins: [
		new MiniCssExtractPlugin({
			filename: env === 'development' ? '[name].css' : '[name]-[chunkhash:6].css',
		}),
		new HtmlWebpackPlugin({
			filename: 'index.html',
			favicon: './src/assets/images/favicon.ico',
			minify: { collapseWhitespace: true },
			template: './src/index.ejs',
			chunks: ['index'],
			inlineSource: '.(js|css)$',
		}),
		new HtmlWebpackPlugin({
			filename: 'app/index.html',
			favicon: './src/assets/images/favicon.ico',
			minify: { collapseWhitespace: true },
			template: './src/app.ejs',
			chunks: ['app'],
			inlineSource: '.css$',
		}),
		new HtmlWebpackInlineSourcePlugin(),
		new WebpackPwaManifest(require('./src/manifest.json')),
	],
	devServer: {
		contentBase: resolve(__dirname, 'src'),
		publicPath: '/',
	},
	devtool: 'inline-source-map',
}, env === 'production' ? require('./webpack.config.production') : {})
