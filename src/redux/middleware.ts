import { ga, trackError } from '../services/analytics'
import * as actions from './actions'

export const errorHandler = () => (next: any) => async (action: any) => {
	try {
		return await next(action)
	} catch (err) {
		trackError(err)
		throw err
	}
}

export const analytics = () => (next: any) => (action: any) => {
	switch (action.type) {
		case String(actions.clearInput):
			ga('send', 'event', 'Keypad', 'Clear')
			break

		case String(actions.setCurrency):
			ga('send', 'event', 'Currency', 'Select', action.payload.currency1 || action.payload.currency2)
			break

		case String(actions.setPermission):
			ga('send', 'event', 'Permission', 'Allow', String(action.payload.locationAllowed))
			break
	}

	return next(action)
}
