import * as config from '../../config'
import { expect } from '../../test/expect'
import * as countries from '../data/countries.json'
import * as countriesCurrencies from '../data/countries_currencies.json'
import { LocationStatus } from '../services/location'
import * as actions from './actions'
import reducer, { initialState } from './reducer'

describe('redux/reducer', () => {
	describe(String(actions.getCurrencyFromLocation), () => {
		it('should update currency1 from payload', () => {
			const position = {
				coords: {
					latitude: 50.0481156,
					longitude: 14.4567761,
				},
			} as any
			const action = actions.getCurrencyFromLocation(countries, countriesCurrencies, position)
			const state = reducer(undefined, action)

			expect(state).to.include({ currency1: 'CZK' })
		})
	})

	describe('LOCATION', () => {
		it('should set locationStatus to searching', () => {
			const action = actions.getLocation.pending()
			const state = reducer(undefined, action)

			expect(state).to.include({ locationStatus: LocationStatus.searching })
		})

		it('should set locationStatus to found', () => {
			const action = actions.getLocation.fulfilled({} as any)
			const state = reducer(undefined, action)

			expect(state).to.include({ locationStatus: LocationStatus.found })
		})

		it('should set locationStatus to failed', () => {
			const action = actions.getLocation.rejected(null)
			const state = reducer(undefined, action)

			expect(state).to.include({ locationStatus: LocationStatus.failed })
		})
	})

	describe(String(actions.getCurrencies.fulfilled), () => {
		it('should convert data from object map to array', () => {
			const action = actions.getCurrencies.fulfilled({
				DKK: 'Danish Krone',
				EUR: 'Euro',
			})
			expect(reducer(undefined, action))
				.to.deep.include({
					currencies: [
						{ id: 'DKK', name: 'Danish Krone' },
						{ id: 'EUR', name: 'Euro' },
					],
				})
		})
	})

	describe(String(actions.setCurrency), () => {
		it('should update currency "from"', () => {
			expect(reducer(undefined, actions.setCurrency('DKK', 1)))
				.to.include({ currency1: 'DKK' })
		})

		it('should update currency "to"', () => {
			expect(reducer(undefined, actions.setCurrency('DKK', 2)))
				.to.include({ currency2: 'DKK' })
		})

		describe('currenciesHistory', () => {
			it('should add currency1 to beginning of array', () => {
				expect(reducer(initialState, actions.setCurrency('NEW', 1)))
					.to.have.nested.property('currenciesHistory[0]', 'NEW')
			})

			it('should add currency2 to beginning of array', () => {
				expect(reducer(initialState, actions.setCurrency('NEW', 2)))
					.to.have.nested.property('currenciesHistory[0]', 'NEW')
			})

			it('should move currency to front of array if already in history', () => {
				const code = initialState.currenciesHistory[2]
				expect(reducer(initialState, actions.setCurrency(code, 1)))
					.to.have.nested.property('currenciesHistory[0]', code)
			})

			it(`should trim history to ${config.currencies.historyLength} entries`, () => {
				let state = reducer()
				for (let i = 0; i <= config.currencies.historyLength; i++) {
					state = reducer(state, actions.setCurrency(`NEW${i + 1}`, i % 2 + 1))
				}
				expect(state.currenciesHistory).to.have.length(config.currencies.historyLength)
			})

			it(`should not duplicate existing entries`, () => {
				const currency2 = initialState.currenciesHistory[1]
				const state = reducer(initialState, actions.setCurrency('NEW', 1))
				expect(state.currenciesHistory.filter((c) => c === currency2)).to.have.length(1)
			})
		})
	})

	describe(String(actions.swapCurrencies), () => {
		it('should swap the currencies', () => {
			const orgState = reducer(undefined, undefined)
			const newState = reducer(orgState, actions.swapCurrencies())

			expect(orgState.currency1).to.eql(newState.currency2)
			expect(orgState.currency2).to.eql(newState.currency1)
		})
	})

	describe(String(actions.setPermission), () => {
		it('should set location allowed', () => {
			expect(reducer(undefined, actions.setPermission(true)))
				.to.contain({ locationAllowed: true })
		})

		it('should set location not allowed', () => {
			expect(reducer(undefined, actions.setPermission(false)))
				.to.contain({ locationAllowed: false })
		})

		it('should set location status disabled', () => {
			expect(reducer(undefined, actions.setPermission(false)))
				.to.contain({ locationStatus: LocationStatus.disabled })
		})

		it('should hide permission dialog on allow', () => {
			const state = reducer(undefined, actions.togglePermissionDialog(true))
			expect(reducer(state, actions.setPermission(true)))
				.to.contain({ showPermissionDialog: false })
		})

		it('should hide permission dialog on deny', () => {
			const state = reducer(undefined, actions.togglePermissionDialog(true))
			expect(reducer(state, actions.setPermission(false)))
				.to.contain({ showPermissionDialog: false })
		})
	})

	describe(String(actions.toggleDrawer), () => {
		it('should toggle the drawer', () => {
			const orgState = reducer(undefined, undefined)
			const newState = reducer(orgState, actions.toggleDrawer() as any)

			expect(orgState.showDrawer).to.not.eql(newState.showDrawer)
		})
	})

	describe(String(actions.toggleShortcutDialog), () => {
		it('should toggle the drawer', () => {
			const orgState = reducer(undefined, undefined)
			const newState = reducer(orgState, actions.toggleShortcutDialog() as any)

			expect(orgState.showShortcuts).to.not.eql(newState.showShortcuts)
		})
	})
})
