import { applyMiddleware, createStore } from 'redux'
import { BoostrappedCallback, persistReducer, persistStore } from 'redux-persist'
import storage from 'redux-persist/lib/storage'
import promiseMiddleware from 'redux-promise-middleware'
import thunkMiddleware from 'redux-thunk'
import { analytics, errorHandler } from './middleware'
import rootReducer from './reducer'

const middleware = [
	analytics,
	errorHandler,
	thunkMiddleware,
	promiseMiddleware,
]

if (process.env.NODE_ENV === 'development') {
	const { logger } = require('redux-logger') // tslint:disable-line
	middleware.push(logger)
}

const persistConfig = {
	key: 'root',
	storage,
}

export default (callback: BoostrappedCallback) => {
	const store = createStore(
		persistReducer(persistConfig, rootReducer as any),
		applyMiddleware(...middleware)
	)

	const persistor = persistStore(store, undefined, callback)

	return { store, persistor }
}
