import { REHYDRATE } from 'redux-persist'
import * as config from '../../config'
import { LocationStatus } from '../services/location'
import { configure } from '../services/money'
import { cloneAsObject } from '../services/object'
import { ICountryCurrencies } from '../services/resources'
import * as actions from './actions'

export interface ICurrency {
	id: string
	name: string
}

export interface IRate {
	id: string
	rate: number
}

export interface IStoreState {
	countries?: any
	country?: { id: string, currency: string }
	countryCurrencies?: ICountryCurrencies
	currencies: ICurrency[]
	currenciesHistory: string[]
	currency1: string
	currency2: string
	currentCurrencyPicker?: number
	locationAllowed?: boolean
	locationStatus: LocationStatus
	position?: Position
	query: string
	rates?: IRate[]
	ratesTimestamp?: number
	showCurrencyPicker: boolean
	showDrawer: boolean
	showInstallDialog: boolean
	showPermissionDialog: boolean
	showShortcuts: boolean
	input: string
}

export const initialState: IStoreState = {
	currencies: [],
	currenciesHistory: config.currencies.selected,
	currency1: config.currencies.default1,
	currency2: config.currencies.default2,
	locationStatus: LocationStatus.searching,
	query: '',
	showCurrencyPicker: false,
	showDrawer: false,
	showInstallDialog: false,
	showPermissionDialog: false,
	showShortcuts: false,
	input: '100',
}

export default (
	state: IStoreState = initialState,
	{ type, payload }: actions.IAction = {} as any
): IStoreState => {
	const merge = (obj: Partial<IStoreState> = {}) => ({ ...state, ...obj })

	switch (type) {
		case REHYDRATE: {
			if (payload && payload.rates) {
				configure({ base: config.currencies.default1, rates: payload.rates })
			}
			return state
		}

		case String(actions.getCurrencies.fulfilled):
			return merge({
				currencies: Object.entries(payload).map(([id, name]) => ({ id, name }) as ICurrency),
			})

		case String(actions.getCurrencyRates.fulfilled):
			return merge({
				rates: payload.rates,
				ratesTimestamp: payload.timestamp * 1000,
			})

		case String(actions.getCountries.fulfilled):
			return merge({ countries: payload })

		case String(actions.getCountryCurrencies.fulfilled):
			return merge({ countryCurrencies: payload })

		case String(actions.setCurrency): {
			const code = payload.currency1 || payload.currency2
			const history = new Set([code, state.currency1, state.currency2, ...state.currenciesHistory])
			const trimmed = Array.from(history).slice(0, config.currencies.historyLength)

			return merge({
				...payload,
				currenciesHistory: trimmed,
			})
		}

		case String(actions.setInput):
		case String(actions.clearInput):
		case String(actions.setLocationStatus):
		case String(actions.setDrawer):
		case String(actions.togglePermissionDialog):
			return merge({ ...payload })

		case String(actions.toggleCurrencyPicker):
			return merge({
				currentCurrencyPicker: payload.number,
				showCurrencyPicker: !state.showCurrencyPicker,
			})

		case String(actions.toggleDrawer):
			return merge({ showDrawer: !state.showDrawer })

		case String(actions.toggleShortcutDialog):
			return merge({ showShortcuts: !state.showShortcuts })

		case String(actions.swapCurrencies):
			return merge({
				currency1: state.currency2,
				currency2: state.currency1,
			 })

		case String(actions.setPermission):
			return merge({
				locationAllowed: payload.locationAllowed,
				locationStatus: payload.locationAllowed ? state.locationStatus : LocationStatus.disabled,
				showPermissionDialog: false,
			})

		case String(actions.getLocation.pending):
			return merge({ locationStatus: LocationStatus.searching })

		case String(actions.getLocation.fulfilled):
			return merge({
				// overwrite timestamp due to Safari bug: http://openradar.appspot.com/9246279
				position: { ...cloneAsObject(payload), timestamp: Date.now() },
				locationStatus: LocationStatus.found,
			})

		case String(actions.getLocation.rejected):
			return merge({ locationStatus: LocationStatus.failed })

		case String(actions.getCurrencyFromLocation): {
			const newState: Partial<IStoreState> = { country: payload }

			const { currency } = payload || ({} as any)
			if (currency && currency !== state.currency2) {
				newState.currenciesHistory = Array.from(new Set(state.currenciesHistory).add(currency))
				newState.currency1 = currency
			}

			return merge(newState)
		}

		case String(actions.search):
			return merge({ query: payload.query })

		case String(actions.toggleInstallDialog):
			return merge({ showInstallDialog: !state.showInstallDialog })

		case String(actions.install):
			return merge({ showInstallDialog: false })

		default:
			return state
	}
}
