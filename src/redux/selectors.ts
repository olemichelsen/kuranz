import { createSelector } from 'reselect'
import { convert } from '../services/money'
import { search } from '../services/search'
import { IStoreState } from './reducer'

const getCurrencies = (state: IStoreState) => state.currencies
const getCurrenciesHistory = (state: IStoreState) => state.currenciesHistory
const getInput = (state: IStoreState) => state.input
const getCurrencyFrom = (state: IStoreState) => state.currency1
const getCurrencyTo = (state: IStoreState) => state.currency2
const getQuery = (state: IStoreState) => state.query
const getRatesTimestamp = (state: IStoreState) => state.ratesTimestamp

export const getConvertedAmount = createSelector(
	getInput,
	getCurrencyFrom,
	getCurrencyTo,
	getRatesTimestamp, // only added to trigger updates
	(input, from, to) => convert(input, from, to).toFixed(2)
)

export const getCommonCurrencies = createSelector(
	getCurrencies,
	getCurrenciesHistory,
	(currencies, history) => history.map((code) => currencies.find(({ id }) => id === code))
)

export const getCurrenciesFiltered = createSelector(
	getCurrencies,
	getQuery,
	(currencies, query) => search(currencies, query, ({ id, name }) => `${id} ${name}`)
)
