import { h, render } from 'preact'
import { Provider } from 'preact-redux'
import * as config from '../config'
import createStore from './redux/store'
import { init as initApp } from './services/app'
import { init as initMobile } from './services/mobile'
import './styles/index.less'

initMobile()

if (process.env.NODE_ENV === 'production') {
	import('./services/analytics').then((analytics) => analytics.init(config.googleAnalytics.id))
	import('./services/offline').then((offline) => offline.init())
}

const { store } = createStore(() => initApp(store))

let root: any

function init() {
	const { ConnectedApp: App } = require('./components/app')
	root = render(
		<Provider store={store}>
			<App />
		</Provider>
	, document.body, root)
}

if (module.hot) {
	module.hot.accept('./components/app', () => requestAnimationFrame(init))
}

init()
