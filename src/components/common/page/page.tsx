import { Component, h } from 'preact'

export class Page extends Component<any, any> {
	private page?: Element

	public componentDidMount() {
		// Prevent content scroll on touch devices
		if ('ontouchstart' in window) {
			this.page!.addEventListener('touchmove', (e) => {
				e.preventDefault()
			})
		}
	}

	public render() {
		return (
			<div className={`page ${this.props.className}`} ref={(elm) => this.page = elm}>
				{this.props.children}
			</div>
		)
	}
}
