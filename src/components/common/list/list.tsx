import { Component, h } from 'preact'
import keycodes from '../../../services/keycodes'
import { shallowEqual } from '../../../services/shallowEqual'
import * as styles from './list.less'

interface IListProps {
	items: Array<{
		id: string
		name: string
	}>
	select: (id: string) => void
}

interface IListState {
	selectedIndex: number
}

export class List extends Component<IListProps, any> {
	private selectedElement?: Element

	constructor(props: IListProps) {
		super(props)
		this.handleKeydown = this.handleKeydown.bind(this)
		this.state = { selectedIndex: 0 }
	}

	public shouldComponentUpdate(props: IListProps, state: IListState) {
		return !(shallowEqual(props, this.props) && shallowEqual(state, this.state))
	}

	public componentDidUpdate(prevProps: IListProps) {
		if (this.props.items.length !== prevProps.items.length) {
			this.setState({ selectedIndex: 0 }, this.scrollSelectedElementIntoView)
		}
	}

	public componentDidMount() {
		document.addEventListener('keydown', this.handleKeydown)
	}

	public componentWillUnmount() {
		document.removeEventListener('keydown', this.handleKeydown)
	}

	public handleKeydown(e: KeyboardEvent) {
		let { selectedIndex } = this.state
		if (e.keyCode === keycodes.enter) {
			e.preventDefault()
			const item = this.props.items[selectedIndex]
			if (item) {
				this.props.select(item.id)
			}
		} else if ([keycodes.down, keycodes.up].includes(e.keyCode)) {
			e.preventDefault()
			selectedIndex += e.keyCode === keycodes.down ? 1 : -1
			if (selectedIndex >= 0 && selectedIndex < this.props.items.length) {
				this.setState({ selectedIndex }, this.scrollSelectedElementIntoView)
			}
		}
	}

	public scrollSelectedElementIntoView() {
		if (this.selectedElement) {
			this.selectedElement.scrollIntoView()
		}
	}

	public render() {
		return (
			<ol className={styles.list}>
				{this.props.items.map((item, i) => (
					<li
						className={i === this.state.selectedIndex ? styles.selected : undefined}
						key={item.id}
						onClick={() => this.props.select(item.id)}
						ref={(elm) => i === this.state.selectedIndex ? this.selectedElement = elm : undefined}
					>
						<span className={styles.code}>{item.id}</span>
						<span className={styles.name}>{item.name}</span>
					</li>
				))}
			</ol>
		)
	}
}
