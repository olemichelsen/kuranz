import { Component, h } from 'preact'
import keycodes from '../../../services/keycodes'
import * as styles from './popup.less'

export interface IPopupProps {
	close: () => void
}

export class Popup extends Component<IPopupProps, any> {
	constructor(props: IPopupProps) {
		super(props)
		this.handleKeydown = this.handleKeydown.bind(this)
	}

	public componentDidMount() {
		document.addEventListener('keydown', this.handleKeydown)
	}

	public componentWillUnmount() {
		document.removeEventListener('keydown', this.handleKeydown)
	}

	public handleKeydown(e: KeyboardEvent) {
		if (e.keyCode === keycodes.esc) {
			e.stopPropagation()
			this.props.close()
		}
	}

	public render() {
		return (
			<div className={styles.popupContainer}>
				<div className={styles.popup}>
					{this.props.children}
				</div>
			</div>
		)
	}
}
