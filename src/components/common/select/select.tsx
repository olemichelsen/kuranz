import { h } from 'preact'
import * as styles from './select.less'

export const Select = (props: any) => {
	const { values, ...rest } = props

	return (
		<div className={styles.select}>
			<select {...rest}>
				{values.map((v: string) => (
					<option>{v}</option>
				))}
			</select>
		</div>
	)
}
