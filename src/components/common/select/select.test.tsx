import { h } from 'preact'
import { expect } from '../../../../test/expect'
import { Select } from './select'

describe('component', () => {
	describe('Select', () => {
		it('should options', () => {
			expect(
				<Select
					values={['A', 'B']}
				/>
			).to.contain(
				<option>A</option>
			).and.to.contain(
				<option>B</option>
			)
		})
	})
})
