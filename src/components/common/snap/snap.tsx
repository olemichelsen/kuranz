import { cloneElement, Component, h } from 'preact'
import * as SnapJS from 'snapjs'

export interface ISnapProps {
	onChange: (state: 'right' | 'closed') => void
	right: JSX.Element
	show: boolean
}

export class Snap extends Component<ISnapProps, any> {
	private snapper: any
	private component?: Component<any, any>

	public componentDidMount() {
		if (this.component) {
			this.snapper = new SnapJS({
				element: this.component.base,
				disable: 'left',
			})
			this.snapper.on(
				'animated',
				() => this.props.onChange(this.snapper.state().state)
			)
		}
	}

	public componentWillReceiveProps({ show }: ISnapProps) {
		if (this.props.show !== show) {
			if (show) {
				this.snapper.open('right')
			} else {
				this.snapper.close()
			}
		}
	}

	public render() {
		return (
			<div>
				<div className="snap-drawers">
					<div className="snap-drawer snap-drawer-right">
						{this.props.right}
					</div>
				</div>
				{cloneElement((this.props.children as any)[0], {
					className: 'snap-content',
					ref: (c: Component<any, any>) => this.component = c,
				})}
			</div>
		)
	}
}
