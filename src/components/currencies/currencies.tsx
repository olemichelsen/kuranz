import { h } from 'preact'
import { ICurrency } from '../../redux/reducer'
import { List } from '../common/list/list'
import * as styles from './currencies.less'

export const Currencies = (props: {
	currenciesCommon: ICurrency[]
	currenciesFiltered: ICurrency[]
	query: string
	search: (query: string) => void
	pickCurrency: (id: string) => void
}) => {
	const handleSearch = ({ target }: any) => {
		if (props.query !== target.value) {
			props.search(target.value)
		}
	}
	const handleSubmit = (event: any) => event.preventDefault()

	return (
		<div className={styles.currencies}>
			<form action="." onSubmit={handleSubmit}>
				<input
					autofocus
					className={styles.queryInput}
					onKeyUp={handleSearch}
					onSearch={handleSearch}
					placeholder="Search for currency"
					type="search"
					value={props.query}
				/>
			</form>
			{!!props.query ?
				<div className={styles.list}>
					<List
						items={props.currenciesFiltered}
						select={props.pickCurrency}
					/>
					{!props.currenciesFiltered.length && <p className={styles.noResults}>No results</p>}
				</div>
				:
				<div className={styles.list}>
					<List
						items={props.currenciesCommon}
						select={props.pickCurrency}
					/>
					<p className={styles.hint}>Search to select from more currencies</p>
				</div>
			}
		</div>
	)
}
