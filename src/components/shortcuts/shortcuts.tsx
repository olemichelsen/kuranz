import { h } from 'preact'
import * as styles from './shortcuts.less'

export const Shortcuts = () => {
	return (
		<ul className={styles.shortcuts}>
			<li>
				<span className="bullet">
					<kbd>ESC</kbd>
				</span>
				Clear value
			</li>
			<li>
				<span className="bullet">
					<kbd>F</kbd>
				</span>
				Select "from" currency
			</li>
			<li>
				<span className="bullet">
					<kbd>T</kbd>
				</span>
				Select "to" currency
			</li>
			<li>
				<span className="bullet">
					<kbd>X</kbd>
				</span>
				Swap currencies
			</li>
		</ul>
	)
}
