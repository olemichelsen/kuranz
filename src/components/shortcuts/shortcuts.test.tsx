import { h } from 'preact'
import { expect } from '../../../test/expect'
import { Shortcuts } from './shortcuts'

describe('shortcuts', () => {
	it('should render shortcut for ESC', () => {
		expect(
			<Shortcuts />
		).to.contain(
			<kbd>ESC</kbd>
		)
	})

	it('should render shortcut for X', () => {
		expect(
			<Shortcuts />
		).to.contain(
			<kbd>X</kbd>
		)
	})
})
