import { h } from 'preact'
import { Key } from './key'
import * as styles from './keypad.less'

export const Keypad = (props: {
	clearValue: () => void
	setValue: (s: string) => void
}) => {
	return (
		<div className={styles.keypad}>
			<div className={styles.row}>
				{['1', '2', '3'].map((v) => (<Key value={v} onClick={props.setValue} tabIndex={Number(v)} />))}
			</div>
			<div className={styles.row}>
				{['4', '5', '6'].map((v) => (<Key value={v} onClick={props.setValue} tabIndex={Number(v)} />))}
			</div>
			<div className={styles.row}>
				{['7', '8', '9'].map((v) => (<Key value={v} onClick={props.setValue} tabIndex={Number(v)} />))}
			</div>
			<div className={styles.row}>
				<Key onClick={props.clearValue} tabIndex={12} value="C" label="Clear" />
				<Key onClick={props.setValue} tabIndex={10} value="0" />
				<Key onClick={props.setValue} tabIndex={11} value="." label="Decimal" />
			</div>
		</div>
	)
}
