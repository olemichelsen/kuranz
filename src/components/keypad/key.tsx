import { h } from 'preact'
import * as styles from './keypad.less'

export const Key = (props: {
	onClick: (s: string) => void
	label?: string
	tabIndex: number
	value: string
}) => {
	return (
		<button
			aria-label={props.label || props.value}
			className={`btn ${styles.key}`}
			data-snap-ignore="true"
			onClick={() => props.onClick(props.value)}
			tabIndex={props.tabIndex}
		>
			{props.value}
		</button>
	)
}
