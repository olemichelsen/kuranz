import { h } from 'preact'
import * as PreactCSSTransitionGroup from 'preact-css-transition-group'
import { connect } from 'preact-redux'
import { bindActionCreators } from 'redux'
import * as actions from '../redux/actions'
import { IStoreState } from '../redux/reducer'
import * as selectors from '../redux/selectors'
import { Page } from './common/page/page'
import { Popup } from './common/popup/popup'
import { Snap } from './common/snap/snap'
import { Currencies } from './currencies/currencies'
import { Header } from './header/header'
import { Install } from './install/install'
import { Keypad } from './keypad/keypad'
import { Permission } from './permission/permission'
import { Settings } from './settings/settings'
import { Shortcuts } from './shortcuts/shortcuts'
import { Values } from './values/values'

export const App = (props: any) => {
	return (
		<div>
			<Snap
				onChange={props.setDrawer}
				show={props.showDrawer}
				right={<Settings {...props} />}
			>
				<Page>
					<Header
						getLocation={props.getLocationAsync}
						locationStatus={props.locationStatus}
						toggleDrawer={props.toggleDrawer}
					/>
					<div className="content">
						<Values {...props} />
						<Keypad
							clearValue={props.clearInput}
							setValue={props.setInput}
						/>
					</div>
				</Page>
			</Snap>
			<PreactCSSTransitionGroup
				transitionName="fadeTransition"
				transitionEnterTimeout={500}
				transitionLeaveTimeout={300}
			>
				{props.showCurrencyPicker && (
					<Popup close={props.toggleCurrencyPicker}>
						<Currencies {...props} />
					</Popup>
				)}
				{props.showPermissionDialog && (
					<Popup close={() => props.setPermission(false)}>
						<Permission setPermission={props.setPermissionAsync} />
					</Popup>
				)}
				{props.showShortcuts && (
					<Popup close={props.toggleShortcutDialog}>
						<Shortcuts />
					</Popup>
				)}
				<Install
					cancel={props.toggleInstallDialog}
					onInstall={props.install}
					onReady={props.toggleInstallDialog}
					show={props.showInstallDialog}
				/>
			</PreactCSSTransitionGroup>
		</div>
	)
}

export const ConnectedApp = connect(
	(state: IStoreState) => ({
		...state,
		currenciesCommon: selectors.getCommonCurrencies(state),
		currenciesFiltered: selectors.getCurrenciesFiltered(state),
		inputConverted: selectors.getConvertedAmount(state),
	}),
	(dispatch: any) => bindActionCreators(actions, dispatch),
	(stateProps, dispatchProps, ownProps) => ({
		...stateProps,
		...dispatchProps,
		...ownProps,
		pickCurrency: (code: string) => {
			dispatchProps.search()
			dispatchProps.setCurrency(code, stateProps.currentCurrencyPicker!)
			dispatchProps.toggleCurrencyPicker(undefined)
		},
	})
)(App)
