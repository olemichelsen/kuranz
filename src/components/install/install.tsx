import { Component, h } from 'preact'
import { Popup } from '../common/popup/popup'
import * as styles from './install.less'

interface IInstallProps {
	cancel: () => void
	onInstall: () => void
	onReady: () => void
	show: boolean
}

interface IInstallEvent extends Event {
	prompt: () => void
	userChoice: Promise<{ outcome: 'accepted' | 'dismissed' }>
}

export class Install extends Component<IInstallProps> {
	private installEvent?: IInstallEvent

	constructor(props: IInstallProps) {
		super(props)
		this.handleBeforeInstallPrompt = this.handleBeforeInstallPrompt.bind(this)
	}

	public componentDidMount() {
		window.addEventListener('beforeinstallprompt', this.handleBeforeInstallPrompt)
	}

	public componentWillUnmount() {
		window.removeEventListener('beforeinstallprompt', this.handleBeforeInstallPrompt)
	}

	public handleBeforeInstallPrompt() {
		window.addEventListener('beforeinstallprompt', (e) => {
			e.preventDefault()
			this.props.onReady()
			this.installEvent = e as IInstallEvent
		})
	}

	public async handleInstall() {
		if (!this.installEvent) {
			return
		}

		this.installEvent.prompt()
		const choice = await this.installEvent.userChoice
		if (choice.outcome === 'accepted') {
			this.props.onInstall()
		}
		this.props.cancel()
		this.installEvent = undefined
	}

	public render() {
		if (!this.props.show) {
			return null
		}

		return (
			<Popup close={this.props.cancel}>
				<div className={styles.install}>
					<p>Add Kuranz to the homescreen</p>
					<button
						className={styles.btnCancel}
						aria-label="Cancel"
						onClick={() => this.props.cancel()}
					>
						Cancel
					</button>
					<button
						className={styles.btnAdd}
						aria-label="Add To Homescreen"
						onClick={() => this.handleInstall()}
					>
						Add
					</button>
				</div>
			</Popup>
		)
	}
}
