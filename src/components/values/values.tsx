import { h } from 'preact'
import { formatCurrency } from '../../services/numbers'
import * as styles from './values.less'

export const Values = (props: {
	currenciesEnabled: string[]
	currency1: string
	currency2: string
	input: string
	inputConverted: string
	setCurrency: (code: string, n: number) => void
	swapCurrencies: () => void
	toggleCurrencyPicker: (n: number) => void
}) => {
	const input = formatCurrency(Number(props.input))
	const value2 = formatCurrency(Number(props.inputConverted))

	const handleClick = (n: number, event: MouseEvent) => {
		event.preventDefault()
		props.toggleCurrencyPicker(n)
	}

	return (
		<div className={styles.values}>
			<div className={`${styles.value} ${styles.value1}`}>
				<span className={styles.amount}>{input}</span>
				<button
					aria-label="Currency from"
					className={styles.btnCurrency}
					onClick={handleClick.bind(null, 1)}
					tabIndex={13}
				>
					{props.currency1}
				</button>
			</div>
			<div className={styles.value}>
				<span className={styles.amount}>{value2}</span>
				<button
					aria-label="Currency to"
					className={styles.btnCurrency}
					onClick={handleClick.bind(null, 2)}
					tabIndex={14}
				>
					{props.currency2}
				</button>
				<button
					aria-label="Swap currencies"
					className={styles.btnSwap}
					onClick={props.swapCurrencies}
					tabIndex={15}
				>
					<i className="icon-arrow-sync" />
				</button>
			</div>
		</div>
	)
}
