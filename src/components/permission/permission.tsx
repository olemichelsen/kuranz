import { h } from 'preact'
import * as styles from './permission.less'

export const Permission = (props: {
	setPermission: (b: boolean) => void
}) => {
	return (
		<div className={styles.permission}>
			<p>To detect the currency of the country you are in, we need access to the GPS in your device.</p>
			<button
				className={styles.btnDeny}
				aria-label="Deny GPS location"
				onClick={() => props.setPermission(false)}
			>
				Deny
			</button>
			<button
				className={styles.btnAllow}
				aria-label="Allow GPS location"
				onClick={() => props.setPermission(true)}
			>
				Allow
			</button>
		</div>
	)
}
