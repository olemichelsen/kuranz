import { h } from 'preact'
import { LocationStatus } from '../../services/location'

export const Status = (props: {
	status: LocationStatus
}) => {
	let icon: any

	switch (props.status) {
		case LocationStatus.disabled:
		case LocationStatus.failed:
		case LocationStatus.unsupported:
			icon = <i className="icon-location-disabled" />
			break
		case LocationStatus.found:
			icon = <i className="icon-my-location" />
			break
		case LocationStatus.searching:
			icon = <i className="icon-location-searching" />
			break
	}

	return icon
}
