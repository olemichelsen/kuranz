import { h } from 'preact'
import { expect } from '../../../test/expect'
import { LocationStatus } from '../../services/location'
import { Status } from './status'

describe('location', () => {
	describe('status', () => {
		([
			[LocationStatus.disabled, 'icon-location-disabled'],
			[LocationStatus.failed, 'icon-location-disabled'],
			[LocationStatus.unsupported, 'icon-location-disabled'],
			[LocationStatus.found, 'icon-my-location'],
			[LocationStatus.searching, 'icon-location-searching'],
		] as Array<[LocationStatus, string]>).forEach(([input, expected]: [LocationStatus, string]) => {
			it(`should render "${expected}" on "${input}"`, () => {
				expect(
					<Status status={input} />
				).to.eql(
					<i className={expected as string} />
				)
			})
		})
	})
})
