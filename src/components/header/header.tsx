import { h } from 'preact'
import { LocationStatus } from '../../services/location'
import { Status } from '../location/status'
import * as styles from './header.less'

export const Header = (props: {
	getLocation: () => void
	locationStatus: LocationStatus
	toggleDrawer: () => void
}) => {
	return (
		<header className={styles.header}>
			<button
				aria-label="Toggle GPS location"
				className={styles.btnLocation}
				onClick={props.getLocation}
				tabIndex={17}
			>
				<Status status={props.locationStatus} />
			</button>
			<h1 className={styles.title}>Kuranz</h1>
			<button
				aria-label="Settings"
				className={styles.btnSettings}
				onClick={props.toggleDrawer}
				tabIndex={16}
			>
				<i className="icon-menu" />
			</button>
		</header>
	)
}
