import { h } from 'preact'
import { expect } from '../../../test/expect'
import { LocationStatus } from '../../services/location'
import { Header } from './header'

describe('component', () => {
	describe('Header', () => {
		it('should render h1 title', () => {
			expect(
				<Header
					getLocation={() => null}
					locationStatus={LocationStatus.searching}
					toggleDrawer={() => null}
				/>
			).to.contain(
				<h1>Kuranz</h1>
			)
		})
	})
})
