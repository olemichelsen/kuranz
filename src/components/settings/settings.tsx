import * as distanceInWords from 'date-fns/distance_in_words'
import { h } from 'preact'
import { LocationStatus } from '../../services/location'
import * as styles from './settings.less'

export const Settings = (props: {
	getCurrencyRates: () => void
	getPositionAsync: () => void
	locationAllowed: boolean
	locationStatus: LocationStatus
	position?: Position
	ratesTimestamp?: number
	setPermission: (allowed: boolean) => void
	toggleCurrency: (id: string) => void
}) => {
	let location = ''
	if (props.locationStatus === LocationStatus.failed) {
		location = 'Location currently unavailable'
	} else if (props.position) {
		location = `Location updated ${distanceInWords(props.position.timestamp, Date.now())} ago`
	}

	let rates = ''
	if (props.ratesTimestamp) {
		rates = `Rates updated ${distanceInWords(props.ratesTimestamp, Date.now())} ago`
	}

	return (
		<div className={styles.settings}>
			<section className={styles.currencies}>
				<h2 className={styles.header}>Location</h2>
				<ol>
					<li onClick={() => props.setPermission(true)}>
						<span className={styles.check}>
							{props.locationAllowed ? <i className="icon-check" /> : null}
						</span>
						<span className={styles.name}>Auto update (1 hr)</span>
					</li>
					<li onClick={() => props.setPermission(false)}>
						<span className={styles.check}>
							{!props.locationAllowed ? <i className="icon-check" /> : null}
						</span>
						<span className={styles.name}>Don't update</span>
					</li>
				</ol>
			</section>
			<section>
				<button className={styles.settingsBtn} onClick={props.getCurrencyRates}>
					Refresh rates
				</button>
				<button className={styles.settingsBtn} onClick={props.getPositionAsync}>
					Refresh location
				</button>
			</section>
			<footer>
				<div>{rates}</div>
				<div>{location}</div>
			</footer>
		</div>
	)
}
