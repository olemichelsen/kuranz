import { findCountryByCoordinates, ICountryDataCollection } from './geotools'
import { ICountryCurrencies } from './resources'

export enum LocationStatus {
	disabled,
	failed,
	found,
	searching,
	unsupported,
}

export const isSupported = () => !!navigator.geolocation

export const getLocation = () => (
	new Promise<Position>((resolve, reject) => {
		navigator.geolocation.getCurrentPosition(resolve, reject)
	})
)

export const getCurrencyFromLocation = (
	countries: ICountryDataCollection,
	countryCurrencies: ICountryCurrencies,
	{ coords }: Position
) => {
	const country = findCountryByCoordinates(countries, coords.latitude, coords.longitude)
	if (country) {
		return {
			id: country.id,
			currency: countryCurrencies[country.id],
		}
	}
}
