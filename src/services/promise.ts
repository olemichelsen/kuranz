export const tap = <T>(promise: Promise<T>, handler: (value: T) => void) =>
	promise.then((value) => {
		handler(value)
		return value
	})
