import { Store } from 'redux'
import * as actions from '../redux/actions'
import { IStoreState } from '../redux/reducer'
import keycodes from '../services/keycodes'
import { parseNumber } from '../services/numbers'

export const init = ({ dispatch, getState }: Store<IStoreState>) => {
	document.addEventListener('paste', (event) => {
		const eventData = (event as ClipboardEvent).clipboardData.getData('Text')
		const pasted = parseNumber(eventData)

		if (!Number.isNaN(pasted)) {
			dispatch(actions.clearInput())
			dispatch(actions.setInput(String(pasted)))
		}
	})

	document.addEventListener('keydown', ({ keyCode: key, target }) => {
		if ((target as any).tagName === 'INPUT') {
			return
		}

		if (keycodes.numeric.includes(key)) {
			dispatch(actions.setInput(String(keycodes.numeric.indexOf(key))))
		} else if ([keycodes.comma, keycodes.dot].includes(key)) {
			dispatch(actions.setInput('.'))
		} else if (key === keycodes.esc) {
			const { showCurrencyPicker, showPermissionDialog, showShortcuts } = getState()
			if (!(showCurrencyPicker || showPermissionDialog || showShortcuts)) {
				dispatch(actions.clearInput())
			}
		} else if (key === keycodes.questionMark) {
			dispatch(actions.toggleShortcutDialog())
		} else if ([keycodes.f, keycodes.t].includes(key)) {
			const { showCurrencyPicker } = getState()
			if (!showCurrencyPicker) {
				dispatch(actions.toggleCurrencyPicker(key === keycodes.f ? 1 : 2))
			}
		} else if (key === keycodes.x) {
			dispatch(actions.swapCurrencies())
		}
	})
}
