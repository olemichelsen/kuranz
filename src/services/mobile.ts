export const init = () => {
	// Init fastclick for Safari Mobile on Homescreen or in WebView
	const isHomescreen = !!(navigator as any).standalone
	const isWebView = /(iPhone|iPod|iPad).*AppleWebKit(?!.*Safari)/i.test(navigator.userAgent)

	if (isHomescreen || isWebView) {
		document.addEventListener('DOMContentLoaded', () => {
			require.ensure(['./safari'], () => null)
		}, false)
	}
}
