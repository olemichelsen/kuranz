// tslint:disable:curly
interface IAnyObject {
	[key: string]: any
}

export const shallowEqual = (a: IAnyObject, b: IAnyObject) => {
	for (const key in a) if (a[key] !== b[key]) return false
	for (const key in b) if (!(key in a)) return false
	return true
}
