import * as config from '../../config'
import * as countries from '../data/countries.json'
import * as countryCurrencies from '../data/countries_currencies.json'
import { ICountryDataCollection } from './geotools'

const fetchJson = <T>(url: string) => fetch(url).then((res) => res.json() as Promise<T>)

export interface IRatesData {
	base: string
	rates: { [country: string]: number }
	timestampe: number
}
export const getCurrencyRates = () => fetchJson<IRatesData>(config.data.rates)

export interface ICurrenciesData {
	[country: string]: string
}
export const getCurrencies = () => fetchJson<ICurrenciesData>(config.data.currencies)

export const getCountries = () => fetchJson<ICountryDataCollection>(countries)

export interface ICountryCurrencies {
	[country: string]: string
}
export const getCountryCurrencies = () => fetchJson<ICountryCurrencies>(countryCurrencies)
