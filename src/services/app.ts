import { Store } from 'redux'
import * as config from '../../config'
import {
	getCountries,
	getCountryCurrencies,
	getCurrencies,
	getCurrencyRates,
	initLocationUpdatesAsync
} from '../redux/actions'
import { IStoreState } from '../redux/reducer'
import { init as initShortcuts } from './shortcuts'

let store: Store<IStoreState>

const startRatesUpdates = () => {
	const { ratesTimestamp } = store.getState()

	if (!ratesTimestamp || Date.now() - ratesTimestamp > config.dataExpire) {
		store.dispatch(getCurrencyRates())
	}

	const ticker = setTimeout(startRatesUpdates, config.dataExpire)

	return () => clearTimeout(ticker)
}

export const init = (s: any) => {
	store = s

	initShortcuts(store)

	store.dispatch(initLocationUpdatesAsync() as any)

	store.dispatch(getCountries())
	store.dispatch(getCountryCurrencies())
	store.dispatch(getCurrencies())

	return startRatesUpdates()
}
