import { search } from '../src/services/search'
import { expect } from './expect'

type IInput = Array<[string, string[], string[]]>

const searchData = [
	'dilbert',
	'deloite',
	'dogbert',
]

describe('search', () => {
	describe('search', () => {
		([
			['', [], []],
			['test', [], []],
			['tests', ['test'], []],
			['ss', ['test'], []],
			['', searchData, searchData],
			['dlrt', searchData, ['dilbert']],
			['be', searchData, ['dilbert', 'dogbert']],
		] as IInput).forEach(([query, data, expected]) => {
			it(`'${query}' should return ${expected.length} results`, () => {
				expect(search(data, query)).to.eql(expected)
			})
		})
	})
})
