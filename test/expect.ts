/// <reference path="../src/typings.d.ts" />
import * as chai from 'chai'
import * as chaiAsPromised from 'chai-as-promised'
import assertJsx from 'preact-jsx-chai'
import * as sinonChai from 'sinon-chai'

chai.use(assertJsx)
chai.use(chaiAsPromised)
chai.use(sinonChai)

export const expect = chai.expect
