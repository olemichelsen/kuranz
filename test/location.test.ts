/* tslint:disable:no-unused-expression */
import * as sinon from 'sinon'
import { getLocation, isSupported } from '../src/services/location'
import { expect } from './expect'

declare const global: any

describe('location', () => {
	describe('isSupported', () => {
		it('should be supported if geolocation API present', () => {
			global.navigator = { geolocation: true }
			expect(isSupported()).to.be.true
		})

		it('should not be supported if geolocation API missing', () => {
			global.navigator = {}
			expect(isSupported()).to.be.false
		})
	})

	describe('getLocation', () => {
		it('should resolve position', () => {
			global.navigator = {
				geolocation: {
					getCurrentPosition: sinon.stub().callsArgWith(0, 'coordinates'),
				},
			}
			return expect(getLocation()).to.eventually.eql('coordinates')
		})

		it('should reject on fail', () => {
			global.navigator = {
				geolocation: {
					getCurrentPosition: sinon.stub().callsArgWith(1, 'error'),
				},
			}
			return expect(getLocation()).to.be.rejectedWith('error')
		})
	})
})
