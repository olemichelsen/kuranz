import { cloneAsObject } from '../src/services/object'
import { expect } from './expect'

describe('object', () => {
	describe('cloneAsObject', () => {
		it('should deep clone object', () => {
			const actual = cloneAsObject({
				sub: { prop: 'key' },
			})
			expect(actual).to.deep.equal({ sub: { prop: 'key' } })
		})
	})
})
