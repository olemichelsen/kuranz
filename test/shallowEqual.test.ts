// tslint:disable:no-unused-expression
import { shallowEqual } from '../src/services/shallowEqual'
import { expect } from './expect'

describe('shallowEqual', () => {
	it('should not be equal if not the same reference', () => {
		expect(shallowEqual({ a: {} }, { a: {} })).to.be.false
	})

	it('should be equal if same reference', () => {
		const a = {}
		expect(shallowEqual({ a }, { a })).to.be.true
	})

	it('should detect missing keys', () => {
		expect(shallowEqual({ a: 1 }, { a: 1, b: 2 })).to.be.false
	})
})
