import * as input from '../src/services/input'
import { expect } from './expect'

describe('input', () => {
	beforeEach(() => {
		input.clearValue()
	})

	describe('setValue', () => {
		it('should set the value to 1', () => {
			expect(input.setValue('1')).to.eql('1')
		})

		it('should append value within timeout range', () => {
			input.setValue('4')
			input.setValue('2')
			expect(input.getValue()).to.eql('42')
		})

		it('should clear value after timeout', () => {
			input.setValue('4', Date.now() - input.clearDelay - 1000)
			input.setValue('2')
			expect(input.getValue()).to.eql('2')
		})

		it('should create decimal on dot after timeout', () => {
			input.setValue('4', Date.now() - input.clearDelay - 1000)
			input.setValue('.')
			expect(input.getValue()).to.eql('0.')
		})

		it('should add dot after zero', () => {
			input.setValue('0')
			input.setValue('.')
			expect(input.getValue()).to.eql('0.')
		})

		it('should add only one dot', () => {
			input.setValue('0')
			input.setValue('.')
			input.setValue('.')
			expect(input.getValue()).to.eql('0.')
		})

		it('should not add dot to decimal number', () => {
			input.setValue('1.2')
			input.setValue('.')
			expect(input.getValue()).to.eql('1.2')
		})
	})

	describe('clearValue', () => {
		it('should set the value to 0', () => {
			input.setValue('42')
			input.clearValue()
			expect(input.getValue()).to.eql('0')
		})
	})
})
