import { formatCurrency, parseNumber } from '../src/services/numbers'
import { expect } from './expect'

declare const global: any

describe('numbers', () => {
	beforeEach(() => {
		global.navigator = { language: 'en-US' }
	})

	describe('formatCurrency', () => {
		[
			[undefined, '0'],
			[42, '42'],
			[12.1212, '12.12'],
			[1234.1234, '1,234.12'],
		].forEach(([input, expected]) => {
			it(`should format "${input}" to ${expected}`, () => {
				expect(formatCurrency(input as number)).to.eql(expected)
			})
		})
	})

	describe('parseNumber', () => {
		[
			[null, 0],
			[undefined, 0],
			['0', 0],
			['1', 1],
			['-1', -1],
			['-1,000.1', -1000.1],
			['1.0', 1],
			['1.1', 1.1],
			['1,000', 1000],
			['1,000.1', 1000.1],
			['1,000,000', 1000000],
			['1,000,000.1', 1000000.1],
		].forEach(([input, expected]) => {
			it(`should parse "${input}" to ${expected}`, () => {
				expect(parseNumber(input as string)).to.eql(expected)
			})
		})
	})
})
