import * as rates from '../src/data/rates.json'
import { configure, convert } from '../src/services/money'
import { expect } from './expect'

describe('money', () => {
	before(() => {
		configure(rates)
	})

	describe('convert', () => {
		[
			['100', 'N/A', 'N/A', '0.00'],
			['100', 'USD', 'EUR', '89.84'],
			['100', 'EUR', 'USD', '111.31'],
			['100', 'USD', 'USD', '100.00'],
			['100', 'EUR', 'DKK', '744.34'],
		].forEach(([value, from, to, expected]) => {
			it(`${value} ${from} should be ${expected} ${to}`, () => {
				expect(convert(value, from, to).toFixed(2)).to.eql(expected)
			})
		})
	})
})
