import { tap } from '../src/services/promise'
import { expect } from './expect'

describe('promise', () => {
	describe('tap', () => {
		it('should return original promise result', () => {
			expect(tap(Promise.resolve(42), () => 'noop')).to.eventually.eql(42)
		})
	})
})
