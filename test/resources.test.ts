import * as sinon from 'sinon'
import * as config from '../config'
import * as resources from '../src/services/resources'
import { expect } from './expect'

declare const global: any

describe('resources', () => {
	beforeEach(() => {
		global.fetch = sinon.stub().returns(Promise.resolve({ json: () => ({}) }))
	})

	describe('getCurrencyRates', () => {
		it('requests openexchangerates.org', () => {
			resources.getCurrencyRates()
			expect(global.fetch).to.have.been.calledWith(config.data.rates)
		})
	})

	describe('getCurrencies', () => {
		it('requests openexchangerates.org', () => {
			resources.getCurrencies()
			expect(global.fetch).to.have.been.calledWith(config.data.currencies)
		})
	})
})
