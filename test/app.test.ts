import * as sinon from 'sinon'
import * as actions from '../src/redux/actions'
import { init } from '../src/services/app'
import { expect } from './expect'

declare const global: any

describe('app', () => {
	let mockStore: any

	before(() => {
		global.document = {
			addEventListener: () => null,
		}
		global.fetch = () => Promise.resolve({ json: () => ({}) })
	})

	beforeEach(() => {
		mockStore = {
			dispatch: sinon.stub(),
			getState: sinon.stub().returns({}),
		}
	})

	describe('init', () => {
		it('should dispatch data calls', () => {
			const off = init(mockStore)
			expect(mockStore.dispatch)
				.to.have.been.calledWith(actions.getCountries())
				.and.to.have.been.calledWith(actions.getCountryCurrencies())
				.and.to.have.been.calledWith(actions.getCurrencies())
			off()
		})
	})
})
