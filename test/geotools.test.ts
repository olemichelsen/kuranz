import * as countries from '../src/data/countries.json'
import { findCountryByCoordinates } from '../src/services/geotools'
import { expect } from './expect'

describe('geotools', () => {
	describe('findByPosition', () => {
		([
			[34.354384, 62.203217, 'Herat', 'Afghanistan'],
			[34.540306, 69.176102, 'Kabul', 'Afghanistan'],
			[34.425974, 70.449142, 'Jalalabad', 'Afghanistan'],
			[47.255772, 11.405869, 'Innsbruck', 'Austria'],
			[48.207887, 16.406708, 'Vienna', 'Austria'],
			[43.861577, 18.432312, 'Sarajevo', 'Bosnia and Herzegovina'],
			[44.781165, 17.207336, 'Banja Luka', 'Bosnia and Herzegovina'],
			[43.336822, 17.818451, 'Mostar', 'Bosnia and Herzegovina'],
			[55.109666, 14.910507, 'Bornholm', 'Denmark'],
			[55.6789126, 12.5899515, 'København', 'Denmark'],
			[55.474506, 8.458443, 'Esbjerg', 'Denmark'],
			[55.281706, 10.329895, 'Fyn', 'Denmark'],
			[43.66157, -79.38858, 'Toronto', 'Canada'],
			[49.256, -123.106613, 'Vancouver', 'Canada'],
			[45.338301, 14.456635, 'Rijka', 'Croatia'],
			[43.516517, 16.469193, 'Split', 'Croatia'],
			[42.778582, 18.93219, 'Niksic', 'Montenegro'],
			[42.421081, 19.260406, 'Podgorica', 'Montenegro'],
			[42.865553, 19.886627, 'Berane', 'Montenegro'],
			[34.001247, 71.542282, 'Peshawar', 'Pakistan'],
			[48.152946, 17.153778, 'Bratislava', 'Slovakia'],
			[40.784092, -73.963051, 'Manhattan', 'United States of America'],
			[43.1004323, 11.7796112, 'Montepulciano', 'Italy'],
			[35.9144663, 14.4029894, 'Marsa', 'Malta'],
			[36.062295, 14.303758, 'Victoria', 'Malta'],
			[1.2902422, 103.8229843, 'Clarke Quay', 'Singapore'],
			[1.2034094, 103.7606943, 'Semaku Island', 'Singapore'],
			[1.3957696, 104.0154336, 'Tekong Island', 'Singapore'],
		] as Array<[number, number, string, string]>).forEach(([lat, lng, city, country]) => {
			it(`${city} should be in ${country}`, () => {
				expect(findCountryByCoordinates(countries, lat, lng)!.name).to.eql(country)
			})
		})
	})
})
